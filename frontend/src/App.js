import React, { useState } from 'react';
import { Routes, Route, Navigate, Link } from 'react-router-dom';
import LoginForm from './components/LoginForm';
import RegistrationForm from './components/RegistrationForm';
import Home from './components/Home';
import ChatRoom from './components/ChatRoom';
import Cake from './components/Cake';
import UserContext from './UserContext';
import ErrorImage from './components/ErrorImage';
import error500Image from './resources/500.png';
import error404Image from './resources/404.png';
import Button from '@mui/material/Button';
import PostList from './components/PostList';


// A custom Element wrapper that checks the authentication status
const PrivateElement = ({ isLoggedIn, children }) => {
  return isLoggedIn ? children : <Navigate to="/login" replace />;
};

const App = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  return (
    <div>
    <UserContext.Provider value={{ username, setUsername, password, setPassword }}>
        <Routes>
        <Route path="/" element={
              <div>
              <Button variant="contained" component={Link} to="/register" style={{ marginRight: '10px' }}>
                  Register
              </Button>
              <Button variant="contained" component={Link} to="/login" style={{ marginRight: '10px' }}>
                  Login
              </Button>
              <Button variant="contained" component={Link} to="/posts" style={{ marginRight: '10px' }}>
                  View Posts
              </Button>
          </div>

            }
          />
          <Route path="/404" element={<ErrorImage src={error404Image} alt="404 Page Not Found" text={`The page is not found.\nGo get some tea, maybe it will appear later.`} />} />
          <Route path="/500" element={<ErrorImage src={error500Image} alt="500 Internal Server Error" text={`Something on the server side went wrong. \n We are working hard to make it worse :)`} />} />
          <Route path="*" element={<Navigate to="/404" replace />} />
          <Route path="/home" element={<PrivateElement isLoggedIn={username}><Home username={username} password={password} /></PrivateElement>} />
          <Route path="/chat" element={<PrivateElement isLoggedIn={username}>{username && <ChatRoom username={username} />}</PrivateElement>} />
          <Route path="/cake" element={<PrivateElement isLoggedIn={username}><Cake /></PrivateElement>} />
          <Route path="/register" element={<RegistrationForm />} />
          <Route path="/login" element={<LoginForm setUsername={setUsername} username={username} />} />
          <Route path="/posts" element={<PostList />} />
        </Routes>
      </UserContext.Provider>
    </div>
  );
};

export default App;
