import { React} from 'react';

const ErrorImage = ({ src, alt, text }) => {
  return (
    <div style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        width: "90vw",
        height: "90vh"
    }}>
      <img src={src} alt={alt} style={{
        maxWidth: '83%',
        maxHeight: '83%',
      }} />
      <div style={{
          width: '80%',
          color: '#333' ,
          fontSize: '4em',
          textAlign:'center',
          fontFamily: 'monospace',
          textShadow: '2px 2px 4px rgba(0, 0, 0, 0.5)'
      }}>
        {text.split('\n').map((line, index) => (
          <span key={index}>
            {line}
            <br />
          </span>
        ))}
      </div>
    </div>
  );
}


export default ErrorImage;
