import React, {useEffect, useState, useContext} from 'react';
import {Client} from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom';
import {config} from "../config";
import { Button } from '@mui/material';

const ChatRoom = () => {
    const {username} = useContext(UserContext);
    const [chatHistory, setChatHistory] = useState(null);
    const [client, setClient] = useState(null);
    const [newMessage, setNewMessage] = useState('');
    const [selectedUser, setSelectedUser] = useState(null);
    const [search, setSearch] = useState('');
    const [userList, setUserList] = useState([]);
    const [searchInput, setSearchInput] = useState('');
    const navigate = useNavigate();
    const [users, setUsers] = useState([]);

    const filteredUsers = userList.filter(user => user && searchInput && user.includes(searchInput));
    const newChatUsers = userList.filter(user => user && !users.includes(user));
    const currentChat = selectedUser && chatHistory.find(chat => chat.username === selectedUser)?.message || []

    useEffect(() => {
        document.querySelectorAll('.user').forEach(function (userElement) {
            userElement.addEventListener('mouseover', function () {
                var hue = Math.floor(Math.random() * 360);
                var saturation = 40;
                var lightness = 70;
                var randomColor = `hsl(${hue}, ${saturation}%, ${lightness}%)`;
                this.style.backgroundColor = randomColor;
            });
        });
    }, [users]);

    useEffect(() => {
        let sock;
        let timeoutId;

        try {
            sock = new SockJS(`${config.chatServiceUrl}/ws`);
            timeoutId = setTimeout(() => {
                if (sock.readyState !== SockJS.OPEN) {
                    console.log('Failed to connect to WebSocket. Make sure the chat-service is running.');
                    navigate('/500');
                }
            }, 3000);
        } catch (error) {
            console.log('Failed to connect to WebSocket. Check the Websocket configuration in chat-service > info.bondar.chat.adapter.websocket.config', error);
            navigate('/500');
            return;
        }
        const stompClient = new Client({
            webSocketFactory: () => sock,
            onConnect: async () => {
                console.log('Connected to WebSocket');

                stompClient.subscribe(`/user/${username}/private`, (message) => {
                    const messageObject = JSON.parse(message.body);
                    console.log('Received: ', messageObject);
                    setChatHistory(prevChatHistory => updateChatHistory(prevChatHistory, messageObject.sender, messageObject));
                });

                try {
                    let response = await fetch(`${config.chatServiceUrl}/api/v1/chat/${username}`);

                    if (!response.ok) {
                        throw new Error(`HTTP error! status: ${response.status}`);
                    }
                    let data = await response.json();
                    
                    let filteredChats = data.chats.filter(chat => chat.username !== null);
                    setChatHistory(filteredChats);
                    setUsers(Array.from(new Set(filteredChats.map(message => message.username))));

                    response = await fetch(`${config.chatServiceUrl}/api/v1/chat/get-users`);
                    if (!response.ok) {
                        throw new Error(`HTTP error! status: ${response.status}`);
                    }
                    data = await response.json();
                    console.log("DATA", data.filter(user => user !== null && user !== username))
                    setUserList(data.filter(user => user !== null && user !== username));
                } catch (error) {
                    console.log(error);
                    if (error.message.includes("404")) {
                        navigate('/404');
                    } else if (error.message.includes("500")) {
                        navigate('/500');
                    }
                }
            },
            onStompError: (frame) => {
                console.log('Failed to connect to WebSocket', frame);
                navigate('/500');
            },
        });

        stompClient.activate();
        setClient(stompClient);

        return () => {
            if (stompClient) {
                stompClient.deactivate();
            }
        };
    }, [username]);

    const sendMessage = async (event) => {
        setNewMessage('');
        event.preventDefault();
        if (client) {
            const message = {"sender": username, "receiver": selectedUser, "message": newMessage};

            console.log("JSON STRINGIFY: ", JSON.stringify(message));
            client.publish({destination: '/app/private-message', body: JSON.stringify(message)})

            try {
                const response = await fetch(`${config.chatServiceUrl}/api/v1/chat/save-message`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(message)
                });

                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                } else {
                    setChatHistory(prevChatHistory => updateChatHistory(prevChatHistory, selectedUser, message));
                }
            } catch (error) {
                console.error('Error:', error);
                if (error.message.includes("404")) {
                    navigate('/404');
                } else if (error.message.includes("500")) {
                    navigate('/500');
                }
            }
        }
    };

    const updateChatHistory = (prevChatHistory, chatWithUsername, message) => {
        const index = prevChatHistory.findIndex(chat => chat.username === chatWithUsername)
        if (index >= 0) {
            const newChatHistory =  [...prevChatHistory]
            newChatHistory[index] = {
                "username": chatWithUsername,
                "message": [...newChatHistory[index].message, message]
            }
            return newChatHistory
        } else {
            setUsers(prevUsers => (prevUsers.indexOf(chatWithUsername) === -1 && [...prevUsers, chatWithUsername]) || prevUsers)
            return [...prevChatHistory, {
                "username": chatWithUsername,
                "message": [message]
            }]
        }
    }

    const selectUser = (user) => {
        setSelectedUser(user);
        setSearchInput('');
    };

    return (
        <div className="chat-container">

            <div className="user-list">
                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'space-around'}}>
                    <h3>Logged in as {username}</h3>
                    <Button type="button" className="send-button" style={{paddingLeft: '10px', paddingRight: '10px'}}
                            onClick={() => navigate('/home')}>Home
                    </Button>
                </div>
                <input className='searchbar' type="text" placeholder="Search..." value={searchInput}
                       onChange={event => setSearchInput(event.target.value)}/>
                <div className="search-results">
                    {filteredUsers.map((user, index) => (
                        <p key={index} onClick={() => selectUser(user)} className="user">{user}</p>
                    ))}
                </div>
                {users.map((user, index) => (
                    <p key={index} onClick={() => selectUser(user)} className="user">{user}</p>
                ))}
                <div className="search-results">
                    {newChatUsers.map((user, index) => (
                        <p key={index} onClick={() => selectUser(user)} className="user">{user}</p>
                    ))}
                </div>
            </div>
            <div className="chat-window">
                {selectedUser && <p>Chat with {selectedUser}</p> }
                <div className="chat-messages">
                    {currentChat.map((message, index) => (
                        <div key={index} className={`message ${message.sender === username ? 'self' : ''}`}>
                            <p>{message.sender}: {message.message}</p>
                        </div>
                    ))}
                </div>
                <form onSubmit={sendMessage} className="message-form">
                    <input type="text" value={newMessage} onChange={event => setNewMessage(event.target.value)}
                           className="message-input"/>
                    <button type="submit" className="send-button">Send</button>
                </form>
            </div>
        </div>
    );
}

export default ChatRoom
