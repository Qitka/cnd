import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { debounce } from 'lodash';

import { config } from '../config';

const sleep = ms => new Promise(r => setTimeout(r, ms));

const PostList = () => {
  const [posts, setPosts] = useState([]);
  const [ratings, setRatings] = useState({});
  const [inputValues, setInputValues] = useState({});

  useEffect(() => {
    axios.get(`${config.postServiceUrl}/api/v1/posts`)
      .then(response => {
        setPosts(response.data);
      })
      .catch(error => {
        console.error('Error fetching posts:', error);
      });

    const fetchRatings = async () => {
      const ratingsData = {};
      await Promise.all(posts.map(async (post) => {
        try {
          const response = await axios.get(`${config.ratingServiceUrl}/api/v1/public/get_rating/${post.id}`);
          ratingsData[post.id] = response.data.average_rating;
        } catch (error) {
          console.error(`Error fetching rating for post ${post.id}:`, error);
          ratingsData[post.id] = null;
        }
      }));
      setRatings(ratingsData);
    };

    const fetchData = async () => {
      await fetchRatings();
      await sleep(5000);
    };

    fetchData();
  }, [posts]);

  const handleRatingInputChange = (postId, event) => {
    const value = event.target.value;
    setInputValues({ ...inputValues, [postId]: value });
  };

  const handleRateButtonClick = async (postId) => {
    const rating = parseInt(inputValues[postId], 10);

    if (isNaN(rating) || rating < 1 || rating > 10) {
      alert('Please enter a valid rating between 1 and 10.');
      return;
    }

    try {
      await axios.post(`${config.ratingServiceUrl}/api/v1/public/rate`, {
        post_id: postId,
        rating: rating,
      });
      alert('Rating added successfully');
    } catch (error) {
      console.error('Error adding rating:', error);
    }
  };

  return (
    <div>
      <h2>Posts</h2>
      <ul>
        {posts.map(post => (
          <li key={post.id}>
            <h3>{post.title}</h3>
            <p>{post.content}</p>
            <small>Posted on: {new Date(post.date).toLocaleDateString()} at {new Date(post.date).toLocaleTimeString()}</small>
            <br />
            <small>by: {post.userRef}</small>
            <br />
            {ratings[post.id] !== undefined ? <small>Average Rating: {ratings[post.id]}</small> : <small>Average Rating: N/A</small>}
            <br />
            <input
              type="number"
              min="1"
              max="10"
              value={inputValues[post.id] || ''}
              onChange={(e) => handleRatingInputChange(post.id, e)}
            />
            <button onClick={() => handleRateButtonClick(post.id)}>Rate</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default PostList;
