import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';


import { config } from "../config";

const Home = ({ username, password }) => {
  console.log("Homepage of user " + username + " " + password);

  const [postData, setPostData] = useState([]);
  const [openCreateDialog, setOpenCreateDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [newPost, setNewPost] = useState({ title: '', content: '', status: '' });
  const [editPost, setEditPost] = useState({ id: null, title: '', content: '', status: '' });

  const handleOpenCreateDialog = () => setOpenCreateDialog(true);
  const handleCloseCreateDialog = () => setOpenCreateDialog(false);
  const handleOpenEditDialog = () => setOpenEditDialog(true);
  const handleCloseEditDialog = () => setOpenEditDialog(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    if (openCreateDialog) {
      setNewPost({ ...newPost, [name]: value });
    } else if (openEditDialog) {
      setEditPost({ ...editPost, [name]: value });
    }
  };

  const fetchPosts = async () => {
    try {
      const response = await fetch(`${config.postServiceUrl}/api/v1/posts/myposts`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          userRef: username,
          authuser: { username: username, password: password },
        }),
      });

      if (response.ok) {
        const data = await response.json();
        setPostData(data);
      } else {
        console.error("Failed to fetch data");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  useEffect(() => {
    fetchPosts();
  }, [username, password]);

  const handlePostClick = () => {
    setNewPost({ title: '', content: '', status: '' });
    handleOpenCreateDialog();
  };

  const handleEditClick = (post) => {
    setEditPost(post);
    handleOpenEditDialog();
  };

  const handleSubmit = async () => {
      try {
        const response = await fetch(`${config.postServiceUrl}/api/v1/posts/`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            title: newPost.title,
            content: newPost.content,
            status: newPost.status,
            userRef: username,
            authuser: { username: username, password: password },
          }),
        });

        if (response.ok) {
          const createdPost = await response.json();
          console.log('Successfully created new post:', newPost);
          handleCloseCreateDialog();
          fetchPosts();
        } else {
          console.error('Failed to create new post:', await response.text());
        }
      } catch (error) {
        console.error('Error submitting new post:', error);
      }
    };

  const handleUpdateSubmit = async () => {
      try {
        const response = await fetch(`${config.postServiceUrl}/api/v1/posts/edit/`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: editPost.id,
            title: editPost.title,
            content: editPost.content,
            status: editPost.status,
            userRef: username,
            authuser: { username: username, password: password },
          }),
        });

        if (response.ok) {
          const createdPost = await response.json();
          console.log('Successfully created new post:', newPost);
          handleCloseEditDialog();
          fetchPosts();
        } else {
          console.error('Failed to create new post:', await response.text());
        }
      } catch (error) {
        console.error('Error submitting new post:', error);
      }
    };

  return (
    <div>
      <h1>Logged in as {username}</h1>
        <Button
          variant="contained"
          component={Link}
          to={{ pathname: "/chat", state: { username: username } }}
          style={{ marginRight: "10px" }}
        >
          Chat
        </Button>
        <Button variant="contained" component={Link} to="/cake" style={{ marginRight: "10px" }}>
          Cake!
        </Button>
      <Button variant="contained" onClick={handlePostClick} style={{ marginRight: "10px" }}>
        Create Post
      </Button>

      <Dialog open={openCreateDialog} onClose={handleCloseCreateDialog}>
        <DialogTitle>Create New Post</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            name="title"
            label="Title"
            type="text"
            fullWidth
            variant="outlined"
            value={newPost.title}
            onChange={handleInputChange}
          />
          <TextField
            margin="dense"
            name="content"
            label="Content"
            type="text"
            fullWidth
            variant="outlined"
            multiline
            rows={4}
            value={newPost.content}
            onChange={handleInputChange}
          />

        <FormControl fullWidth margin="dense">
          <InputLabel>Status</InputLabel>
          <Select
            name="status"
            value={newPost.status}
            label="Status"
            onChange={handleInputChange}
          >
            <MenuItem value="PUBLISHED">Published</MenuItem>
            <MenuItem value="DRAFT">Draft</MenuItem>
          </Select>
        </FormControl>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseCreateDialog}>Cancel</Button>
          <Button onClick={handleSubmit}>Create</Button>
        </DialogActions>
      </Dialog>

      <h2>My Posts</h2>
      <ul>
        {postData.map((post) => (
          <li key={post.id}>
            <h3>{post.title}</h3>
            <p>{post.content}</p>
            <p>{post.status}</p>
            <Button
              variant="contained"
              onClick={() => handleEditClick(post)}
              style={{ marginTop: '10px' }}
            >
              Edit
            </Button>
          </li>
        ))}
      </ul>

      <Dialog open={openEditDialog} onClose={handleCloseEditDialog}>
        <DialogTitle>Edit Post</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            name="title"
            label="Title"
            type="text"
            fullWidth
            variant="outlined"
            value={editPost.title}
            onChange={handleInputChange}
          />
          <TextField
            margin="dense"
            name="content"
            label="Content"
            type="text"
            fullWidth
            variant="outlined"
            multiline
            rows={4}
            value={editPost.content}
            onChange={handleInputChange}
          />
        <FormControl fullWidth margin="dense">
          <InputLabel>Status</InputLabel>
          <Select
            name="status"
            value={newPost.status}
            label="Status"
            onChange={handleInputChange}
          >
            <MenuItem value="PUBLISHED">Published</MenuItem>
            <MenuItem value="DRAFT">Draft</MenuItem>
          </Select>
        </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseEditDialog}>Cancel</Button>
          <Button onClick={handleUpdateSubmit}>Update</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default Home;
