import React from "react";
import { Link} from "react-router-dom";
import Button from '@mui/material/Button';

const Home = () => {
    return(
        <div>
            <h1>...is a lie!</h1>
            <Button variant="contained" component={Link} to="/404" style={{ marginRight: '10px' }}>
                404
            </Button>
            <Button variant="contained" component={Link} to="/500">
                500
            </Button>
        </div>
    )
    
}

export default Home;