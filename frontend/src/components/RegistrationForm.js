import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, TextField, Button } from '@mui/material';
import {config} from "../config";


const RegistrationForm = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const navigate = useNavigate();

    const handleRegister = async () => {
        try {
        console.log("Data: " + JSON.stringify({ username, password, email }));
            const response = await fetch(`${config.userServiceUrl}/api/v1/user/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ username, password, email }),
            });

            if (response.ok) {
                console.log('Registration successful');
                
                navigate('/login');
            } else {
                if(response.status == '404'){
                    console.error('Registration failed. Check if the user-service and chat-service are running.');
                    navigate('/404');
                }else{
                    console.error('An internal server error occured. Most likely the registered username already exists in the table :)');
                    navigate('/500');
                }
                
            }
        } catch (error) {
            console.log(error);
            console.error('An error occurred:', error);
            navigate('/500');
        }
    }

    return (
        <Box component="div" display="flex" flexDirection="row" alignItems="center">
            <Box mr={2}>
                <TextField className='input-elem' placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)} />
            </Box>
            <Box mr={2}>
                <TextField className='input-elem' type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </Box>
            <Box mr={2}>
                <TextField className='input-elem' type="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
            </Box>
            <Box mr={2}>
                <Button className='input-elem' variant="contained" onClick={handleRegister}>Register</Button>
            </Box>
        </Box>
    );
};

export default RegistrationForm;
