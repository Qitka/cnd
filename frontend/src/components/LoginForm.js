import React, { useState, useContext } from 'react';
import { useNavigate , Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Box, TextField, Button } from '@mui/material';
import {config} from "../config";


const LoginForm = ({ }) => {
    const { username, setUsername, password, setPassword } = useContext(UserContext);
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();

    const handleLogin = async () => {
        const response = await fetch(`${config.userServiceUrl}/api/v1/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
        });
        if (response.ok) {
            const un = await response.text();
            console.log('Login successful');
            console.log(un);
            setUsername(un);
            setPassword(password)
            console.log(password);
            navigate('/home', { state: { username: un, password: password} });
        } else {
            const errorText = await response.text();
            setErrorMessage("Username and/or password are incorrect.");
            console.error('Login failed: ' + errorText);
            if (response.status === 404) {
                navigate('/404');
            } else if (response.status >= 500 && response.status < 600) {
                navigate('/500');
            }
        }
    };

    return (
        <Box component="div" display="flex" flexDirection="row" alignItems="center">
            <Box mr={2}>
                <TextField className='input-elem' variant="outlined" placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)} />
            </Box>
            <Box mr={2}>
                <TextField  className='input-elem' variant="outlined" type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </Box>
            <Box mr={2}>
                <Button variant="contained" onClick={handleLogin}>
                    Login
                </Button>
            </Box>
            <Box mr={2}>
                <Button variant="contained" component={Link} to="/register">
                    Register
                </Button>
            </Box>
            {errorMessage && <div>{errorMessage}</div>}
        </Box>
    );
};


export default LoginForm;
