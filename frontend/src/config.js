
export const config = {
    userServiceUrl: process.env.REACT_APP_USER_URL || "http://localhost:9191",
    emailServiceUrl: process.env.REACT_APP_EMAIL_URL || "http://localhost:9192",
    chatServiceUrl: process.env.REACT_APP_CHAT_URL || "http://localhost:9193",
    postServiceUrl: process.env.REACT_APP_POST_URL || "http://localhost:9194",
    ratingServiceUrl: process.env.REACT_APP_RATING_URL || "http://localhost:5000"
}
