package info.bondar.user;

import info.bondar.user.controller.UserController;
import info.bondar.user.entity.User;
import info.bondar.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class UserControllerTest {

    UserService userService = mock();

    UserController userController = new UserController(userService);

    @Test
    void registerUser() {
        User user = new User();
        when(userService.registerUser(any(User.class))).thenReturn(user);

        ResponseEntity<User> result = userController.registerUser(user);

        verify(userService, times(1)).registerUser(user);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(user, result.getBody());
    }

    @Test
    void loginUser() {
        String username = "testUser";
        String password = "testPassword";
        User user = new User();
        when(userService.loginUser(username, password)).thenReturn(user);

        Map<String, String> loginDetails = new HashMap<>();
        loginDetails.put("username", username);
        loginDetails.put("password", password);

        ResponseEntity<String> result = userController.loginUser(loginDetails);

        verify(userService, times(1)).loginUser(username, password);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(username, result.getBody());
    }

    @Test
    void getAllUsers() {
        User user1 = new User();
        User user2 = new User();
        when(userService.getAllUsers()).thenReturn(Arrays.asList(user1, user2));

        ResponseEntity<List<User>> result = userController.getAllUsers();

        verify(userService, times(1)).getAllUsers();
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(Arrays.asList(user1, user2), result.getBody());
    }
}
