package info.bondar.user;
import info.bondar.user.entity.User;
import info.bondar.user.repository.UserRepository;
import info.bondar.user.service.ChatService;
import info.bondar.user.service.EmailService;
import info.bondar.user.service.UserService;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class UserServiceTest {

    UserRepository userRepository = mock();

    EmailService emailService = mock();

    ChatService chatService = mock();

    UserService userService = new UserService(userRepository, emailService, chatService);


    @Test
    void registerUser() {
        User user = new User();

        when(userRepository.save(user)).thenReturn(user);
        doNothing().when(emailService).sendRegistrationEmail(any());
        doNothing().when(chatService).updateUsersInChatService(user);

        User result = userService.registerUser(user);

        verify(userRepository, times(1)).save(user);

        assertEquals(user, result);
    }

    @Test
    void loginUser() {

        String username = "testUser";
        String password = "testPassword";

        User user = new User();
        user.setUsername(username);
        user.setPassword(password);

        when(userRepository.findByUsername(username)).thenReturn(user);

        User resultSuccess = userService.loginUser(username, password);

        User resultFail = userService.loginUser(username, "wrongPassword");

        verify(userRepository, times(2)).findByUsername(username);

        assertEquals(user, resultSuccess);
        assertNull(resultFail);
    }

    @Test
    void getAllUsers() {
        User user1 = new User();
        User user2 = new User();
        when(userRepository.findAll()).thenReturn(Arrays.asList(user1, user2));

        List<User> result = userService.getAllUsers();

        verify(userRepository, times(1)).findAll();
        assertEquals(Arrays.asList(user1, user2), result);
    }
}
