package info.bondar.user.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import info.bondar.user.entity.User;

@Component
public class ChatService {

    private final RestTemplate restTemplate;

    private final String chatServiceUrl;

    public ChatService(RestTemplate restTemplate, @Value("${services.chat-service}") String chatServiceUrl) {
        this.restTemplate = restTemplate;
        this.chatServiceUrl = chatServiceUrl;
    }

    public void updateUsersInChatService(User savedUser) {
        try {
            HttpEntity<User> userBody = new HttpEntity<>(savedUser);
            restTemplate.postForEntity(chatServiceUrl + "/api/v1/chat/users", userBody, String.class);
            System.out.println("User updated in chat service");
        } catch (Exception e) {
            System.out.println("Failed to update user in chat service: " + e.getMessage());
        }
    }

}
