package info.bondar.user.service;

import java.util.concurrent.CompletableFuture;

import info.bondar.user.entity.User;
import info.bondar.user.model.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class EmailService {

    private final RestTemplate restTemplate;

    private final String emailServiceUrl;

    public EmailService(RestTemplate restTemplate, @Value("${services.email-service}") String emailServiceUrl) {
        this.restTemplate = restTemplate;
        this.emailServiceUrl = emailServiceUrl;
    }

    public void sendRegistrationEmail(User user) {
        CompletableFuture.runAsync(() -> { // Sending email takes long time, so do async / non-blocking
            try {
                HttpEntity<Email> emailBody = new HttpEntity<>(new Email(
                    user.getEmail(),
                    "Welcome " + user.getUsername(),
                    "You have sucessfully registered"
                ));
                restTemplate.postForEntity(emailServiceUrl + "/api/v1/email/send", emailBody, String.class);
            } catch (Exception e) {
                System.out.println("Failed to send email: " + e.getMessage());
            }
        });
    }

}
