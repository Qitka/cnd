package info.bondar.user.service;

import info.bondar.user.entity.User;
import info.bondar.user.repository.UserRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final EmailService emailService;

    private final ChatService chatService;

    public UserService(UserRepository userRepository, EmailService emailService, ChatService chatService) {
        this.userRepository = userRepository;
        this.emailService = emailService;
        this.chatService = chatService;
    }

    public User registerUser(User user) {
        User savedUser = userRepository.save(user);
        emailService.sendRegistrationEmail(user);
        chatService.updateUsersInChatService(savedUser);
        return savedUser;
    }

    public User loginUser(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
