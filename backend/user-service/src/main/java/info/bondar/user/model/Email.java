package info.bondar.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Email {
    private String email;
    private String subject;
    private String message;
}
