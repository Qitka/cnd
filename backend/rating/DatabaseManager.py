import sqlite3


class DatabaseManager:
    def __init__(self, db_path):
        self.db_path = db_path
        self.create_tables()

    def create_tables(self):
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS ratings (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        post_id INTEGER NOT NULL,
                        rating INTEGER NOT NULL
                    )''')
        conn.commit()
        conn.close()

    def add_rating(self, post_id, rating):
        print("add_rating db")
        conn = sqlite3.connect(self.db_path)
        print("con created")
        c = conn.cursor()
        c.execute('INSERT INTO ratings (post_id, rating) VALUES (?, ?)', (post_id, rating))
        conn.commit()
        conn.close()

    def get_average_rating(self, post_id):
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        c.execute('SELECT AVG(rating) FROM ratings WHERE post_id = ?', (post_id,))
        result = c.fetchone()
        conn.close()
        return result
