import os
import shutil
import unittest
from app import RatingApp


class TestApp(unittest.TestCase):

    def setUp(self):
        self.app = RatingApp('storage/ratings_test.db').app.test_client()
        self.app.testing = True

    def test1_rate_post(self):
        response = self.app.post('/api/v1/public/rate', json={
            'post_id': 1,
            'rating': 5
        })
        self.assertEqual(response.status_code, 201)
        self.assertIn('Rating added successfully', response.data.decode())

    def test2_rate_post_rating_too_big(self):
        response = self.app.post('/api/v1/public/rate', json={
            'post_id': 1,
            'rating': 11
        })
        self.assertEqual(response.status_code, 400)
        self.assertIn('Rating must be in range 1 to 10', response.data.decode())

    def test3_rate_post_rating_too_small(self):
        response = self.app.post('/api/v1/public/rate', json={
            'post_id': 1,
            'rating': -1
        })
        self.assertEqual(response.status_code, 400)
        self.assertIn('Rating must be in range 1 to 10', response.data.decode())

    def test4_get_rating(self):
        response = self.app.get('/api/v1/public/get_rating/1')
        self.assertEqual(response.status_code, 200)

    def test5_get_rating_not_found(self):
        response = self.app.get('/api/v1/public/get_rating/0x0x0x0x0x0xDoesNotExist0x0x0x0x0x0x')
        self.assertEqual(response.status_code, 404)


if __name__ == '__main__':
    shutil.copy2('storage/ratings.db', 'storage/ratings_test.db')
    unittest.main()
    os.remove('storage/ratings_test.db')
