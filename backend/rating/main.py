from flask import Flask

from DatabaseManager import DatabaseManager
from app import AppRoutes


class RatingApp:
    def __init__(self):
        self.app = Flask(__name__)
        self.db_manager = DatabaseManager('storage/ratings.db')
        self.app_routes = AppRoutes(self.app, self.db_manager)

    def run(self):
        self.app.run(debug=True, host='0.0.0.0', port=5000)


if __name__ == '__main__':
    app = RatingApp()
    app.run()
