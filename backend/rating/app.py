from flask import Flask, jsonify, request
from DatabaseManager import DatabaseManager
import unittest


class AppRoutes:
    def __init__(self, app, db_manager):
        self.app = app
        self.db_manager = db_manager
        self.api_version = 'v1'
        self.api_url = f'api/{self.api_version}/public'

        @app.route('/health')
        def health_check():
            return 'OK'

        @app.route(f'/{self.api_url}/rate', methods=['POST'])
        def rate_post():
            data = request.get_json()
            post_id = data.get('post_id')
            rating = data.get('rating')

            if not post_id or not rating:
                return jsonify({'error': 'Missing post_id or rating'}), 400

            if not isinstance(rating, int):
                return jsonify({'error': 'Rating must be an integer'}), 400

            if rating <= 0 or rating > 10:
                return jsonify({'error': 'Rating must be in range 1 to 10'}), 400

            self.db_manager.add_rating(post_id, rating)
            return jsonify({'message': 'Rating added successfully'}), 201

        @self.app.route(f'/{self.api_url}/get_rating/<post_id>', methods=['GET'])
        def get_rating(post_id):
            result = self.db_manager.get_average_rating(post_id)

            if result is None or result[0] is None:
                return jsonify({'error': 'Post not found'}), 404

            avg_rating = round(result[0], 1)
            return jsonify({'post_id': post_id, 'average_rating': avg_rating}), 200


class RatingApp:
    def __init__(self, db_name):
        self.app = Flask(__name__)
        self.db_manager = DatabaseManager(db_name)
        self.app_routes = AppRoutes(self.app, self.db_manager)

    def run(self):
        self.app.run(debug=True, host='0.0.0.0', port=5000)


if __name__ == '__main__':
    test_suite = unittest.TestLoader().discover('.', pattern='test*.py')
    test_result = unittest.TextTestRunner().run(test_suite)

    if test_result.wasSuccessful():
        print("Tests passed. Starting the Flask application...")
        app = RatingApp('storage/ratings.db')
        app.run()
    else:
        print("Tests failed. Flask application not started.")
