package de.thi.inf.sesa.hexa.test;

import de.thi.inf.sesa.hexa.adapter.api.rest.PostController;
import de.thi.inf.sesa.hexa.adapter.api.rest.dto.*;
import de.thi.inf.sesa.hexa.domain.AuthService;
import de.thi.inf.sesa.hexa.domain.PostService;
import de.thi.inf.sesa.hexa.domain.Status;
import de.thi.inf.sesa.hexa.domain.model.AuthUser;
import de.thi.inf.sesa.hexa.domain.model.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class PostControllerTest {

    private PostService postService;
    private AuthService authService;
    private PostController postController;

    @BeforeEach
    void setUp() {
        postService = mock(PostService.class);
        authService = mock(AuthService.class);
        postController = new PostController(postService, authService);
    }

    @Test
    void createPost() {
        // Arrange
        CreatePostRequest request = new CreatePostRequest();
        request.setAuthuser(new AuthUser("postUser1", "DoNotReadThis321!"));
        request.setTitle("Hello World!");
        request.setContent("Hello Content!");
        request.setStatus(Status.PUBLISHED);

        Post post = new Post();
        post.setTitle(request.getTitle());
        post.setContent(request.getContent());
        post.setStatus(request.getStatus());
        post.setUserRef(request.getAuthuser().getUsername());

        when(authService.authenticate(null, request.getAuthuser().getUsername(), request.getAuthuser().getPassword()))
                .thenReturn(HttpStatus.OK);
        when(postService.createPost(anyString(), anyString(), any(), anyString()))
                .thenReturn(post);

        // Act
        ResponseEntity<PostResponse> response = postController.createPost(request);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(post.getTitle(), response.getBody().getTitle());
        assertEquals(post.getContent(), response.getBody().getContent());
        assertEquals(post.getStatus(), response.getBody().getStatus());
        assertEquals(post.getUserRef(), response.getBody().getUserRef());
    }

    @Test
    void createVeryLongPost() {
        // Arrange
        CreatePostRequest request = new CreatePostRequest();
        request.setAuthuser(new AuthUser("postUser1", "DoNotReadThis321!"));
        request.setTitle("Hello World!");
        request.setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet dictum sit amet justo donec enim diam. Vel facilisis volutpat est velit egestas dui id. Morbi tristique senectus et netus et malesuada fames. Sed faucibus turpis in eu mi bibendum neque egestas. Rutrum quisque non tellus orci ac auctor augue mauris augue. Elit duis tristique sollicitudin nibh sit amet commodo nulla. In iaculis nunc sed augue lacus viverra vitae. Facilisis leo vel fringilla est ullamcorper. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. Pulvinar sapien et ligula ullamcorper malesuada proin. Dui sapien eget mi proin sed libero enim sed faucibus. Laoreet sit amet cursus sit amet dictum sit amet. Ullamcorper velit sed ullamcorper morbi. Id ornare arcu odio ut sem nulla pharetra. Lacus vel facilisis volutpat est velit egestas dui id ornare. Amet volutpat consequat mauris nunc congue nisi. Sit amet consectetur adipiscing elit ut aliquam purus sit. Suscipit tellus mauris a diam maecenas sed. Semper risus in hendrerit gravida rutrum.\n" +
                "\n" +
                "Gravida in fermentum et sollicitudin ac orci phasellus egestas tellus. Dictum sit amet justo donec enim diam. Vitae ultricies leo integer malesuada nunc vel risus. Sollicitudin nibh sit amet commodo nulla facilisi nullam. Sagittis aliquam malesuada bibendum arcu. Senectus et netus et malesuada fames ac turpis egestas maecenas. Fermentum et sollicitudin ac orci phasellus. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec pretium. Nisl nunc mi ipsum faucibus vitae. Nulla facilisi morbi tempus iaculis urna id volutpat. Dui faucibus in ornare quam. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Tellus elementum sagittis vitae et leo duis.\n" +
                "\n" +
                "Eget mauris pharetra et ultrices neque ornare aenean euismod elementum. Pharetra vel turpis nunc eget lorem dolor sed. Hac habitasse platea dictumst quisque sagittis purus sit amet volutpat. Neque aliquam vestibulum morbi blandit. Mauris augue neque gravida in fermentum et sollicitudin ac. Vel pretium lectus quam id leo in. Libero id faucibus nisl tincidunt eget nullam non nisi est. Quam adipiscing vitae proin sagittis nisl rhoncus. Dictum non consectetur a erat nam at lectus. In cursus turpis massa tincidunt dui ut ornare lectus. Euismod elementum nisi quis eleifend. Ornare arcu dui vivamus arcu felis bibendum. Gravida quis blandit turpis cursus in. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi. Mi proin sed libero enim sed. Molestie ac feugiat sed lectus vestibulum. Vel turpis nunc eget lorem dolor sed viverra ipsum. Enim nunc faucibus a pellentesque. Quam pellentesque nec nam aliquam sem et tortor consequat id.\n" +
                "\n" +
                "Elementum curabitur vitae nunc sed. In dictum non consectetur a erat nam at lectus urna. Sed velit dignissim sodales ut eu. Viverra adipiscing at in tellus integer feugiat scelerisque varius morbi. Imperdiet sed euismod nisi porta lorem mollis aliquam. Auctor augue mauris augue neque gravida. Sit amet consectetur adipiscing elit duis tristique sollicitudin. Urna cursus eget nunc scelerisque viverra mauris in. Vitae nunc sed velit dignissim sodales ut eu sem integer. Amet consectetur adipiscing elit ut aliquam. At in tellus integer feugiat scelerisque. Scelerisque in dictum non consectetur a erat nam at. Vestibulum mattis ullamcorper velit sed ullamcorper morbi.\n" +
                "\n" +
                "Orci porta non pulvinar neque laoreet suspendisse. Nisi lacus sed viverra tellus in hac habitasse. Tellus molestie nunc non blandit massa. Id consectetur purus ut faucibus pulvinar elementum. Consequat mauris nunc congue nisi vitae suscipit tellus mauris a. Sit amet commodo nulla facilisi nullam vehicula ipsum. Elit ullamcorper dignissim cras tincidunt lobortis feugiat. Id venenatis a condimentum vitae sapien pellentesque. Enim nunc faucibus a pellentesque sit amet porttitor. Placerat vestibulum lectus mauris ultrices. Volutpat consequat mauris nunc congue. Eros donec ac odio tempor orci dapibus ultrices in. Proin nibh nisl condimentum id venenatis a condimentum vitae. Dolor sit amet consectetur adipiscing elit ut. Id consectetur purus ut faucibus pulvinar. Nisl tincidunt eget nullam non. Varius vel pharetra vel turpis nunc eget!");
        request.setStatus(Status.PUBLISHED);

        Post post = new Post();
        post.setTitle(request.getTitle());
        post.setContent(request.getContent());
        post.setStatus(request.getStatus());
        post.setUserRef(request.getAuthuser().getUsername());

        when(authService.authenticate(null, request.getAuthuser().getUsername(), request.getAuthuser().getPassword()))
                .thenReturn(HttpStatus.OK);
        when(postService.createPost(anyString(), anyString(), any(), anyString()))
                .thenReturn(post);

        // Act
        ResponseEntity<PostResponse> response = postController.createPost(request);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(post.getTitle(), response.getBody().getTitle());
        assertEquals(post.getContent(), response.getBody().getContent());
        assertEquals(post.getStatus(), response.getBody().getStatus());
        assertEquals(post.getUserRef(), response.getBody().getUserRef());
    }

    @Test
    void editPost() {
        // Arrange
        Post post = new Post();
        post.setTitle("Initial Title");
        post.setContent("Initial Content");
        post.setStatus(Status.PUBLISHED);
        post.setUserRef("username");
        UUID postId = post.getId();

        EditPostRequest request = new EditPostRequest();
        request.setId(postId);
        request.setTitle("New Title");
        request.setContent("New Content");
        request.setStatus(Status.PUBLISHED);
        request.setAuthuser(new AuthUser("username", "password"));

        when(authService.authenticateUserAndPost(eq(postId), eq("username"), eq("password"))).thenReturn(HttpStatus.OK);
        when(postService.editPost(eq(postId), eq("New Title"), eq("New Content"), eq(Status.PUBLISHED), eq("username"))).thenAnswer(invocation -> {
            post.setTitle(request.getTitle());
            post.setContent(request.getContent());
            return post;
        });

        // Act
        ResponseEntity<PostResponse> response = postController.editPost(request);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("New Title", response.getBody().getTitle());
        assertEquals("New Content", response.getBody().getContent());
    }

    @Test
    void listPostsByUser() {
        // Arrange
        ListPostsByUserRequest request = new ListPostsByUserRequest();
        request.setAuthuser(new AuthUser("username", "password"));

        when(authService.authenticate(eq(null), eq("username"), eq("password"))).thenReturn(HttpStatus.OK);

        // Act
        ResponseEntity<List<PostResponse>> response = postController.listPostsByUser(request);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
