package de.thi.inf.sesa.hexa.application;

import de.thi.inf.sesa.hexa.domain.Status;
import de.thi.inf.sesa.hexa.domain.model.Post;
import de.thi.inf.sesa.hexa.domain.PostService;
import de.thi.inf.sesa.hexa.ports.outgoing.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository posts;

    @Override
    public Post createPost(String title, String content, Status status, String userRef) {
        Post post = new Post();
        post.setTitle(title);
        post.setContent(content);
        post.setStatus(status);
        post.setUserRef(userRef);
        post.setDate(LocalDateTime.now());
        post.setValidUntil(LocalDateTime.now().plusDays(30));

        posts.save(post);
        return post;
    }

    @Override
    public Post editPost(UUID id, String title, String content, Status status, String userRef) {
        Post post = findPost(id);
        post.setTitle(title);
        post.setContent(content);
        post.setStatus(status);
        post.setUserRef(userRef);
        post.setDate(LocalDateTime.now());

        posts.save(post);
        return post;
    }

    @Override
    public Post changeStatus(UUID id, Status status, String userRef) {
        Post post = findPost(id);
        post.setStatus(status);
        post.setUserRef(userRef);

        posts.save(post);
        return post;
    }

    @Override
    public Post findPost(UUID id) {
        return posts.findById(id);
    }

    @Override
    public List<Post> getPosts() {
        return posts.listAll();
    }
}