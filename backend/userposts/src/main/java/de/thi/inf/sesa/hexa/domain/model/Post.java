package de.thi.inf.sesa.hexa.domain.model;

import de.thi.inf.sesa.hexa.domain.Status;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor

public class Post {
    @Setter(AccessLevel.NONE)
    private UUID id;
    private String title;
    private String content;
    private Status status;
    private LocalDateTime date;
    private LocalDateTime validUntil;
    private String userRef;

    public Post() {
        this.id = UUID.randomUUID();
    }
}