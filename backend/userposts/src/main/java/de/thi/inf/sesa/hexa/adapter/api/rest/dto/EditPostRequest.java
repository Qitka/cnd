package de.thi.inf.sesa.hexa.adapter.api.rest.dto;

import de.thi.inf.sesa.hexa.domain.Status;
import de.thi.inf.sesa.hexa.domain.model.AuthUser;
import de.thi.inf.sesa.hexa.domain.model.UserReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor

public class EditPostRequest {
    private UUID id;
    private AuthUser authuser;
    private String title;
    private String content;
    private Status status;
}