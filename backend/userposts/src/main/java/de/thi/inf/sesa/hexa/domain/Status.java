package de.thi.inf.sesa.hexa.domain;

public enum Status {
    DRAFT,
    PUBLISHED,
    REMOVED,
    EXPIRED
}