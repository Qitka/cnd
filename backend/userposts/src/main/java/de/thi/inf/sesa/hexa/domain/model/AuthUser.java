package de.thi.inf.sesa.hexa.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

@Data
@AllArgsConstructor
public class AuthUser {
    private String username;
    private String password;
}