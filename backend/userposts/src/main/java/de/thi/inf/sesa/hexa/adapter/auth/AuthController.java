package de.thi.inf.sesa.hexa.adapter.auth;

import de.thi.inf.sesa.hexa.domain.PostService;
import de.thi.inf.sesa.hexa.domain.model.Post;
import de.thi.inf.sesa.hexa.ports.outgoing.AuthAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class AuthController implements AuthAdapter {
    private final String authServiceURL;

    private final RestTemplate restTemplate;

    @Autowired
    private PostService postService;

    public AuthController(RestTemplate restTemplate, @Value("${services.user-service}") String authServiceURL) {
        this.restTemplate = restTemplate;
        this.authServiceURL = authServiceURL;
    }



    @Override
    public HttpStatus authenticateUser(String username, String password) {
        try {
            String auth_api_url = authServiceURL + "/api/v1/user/login";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            Map<String, String> jsonMap = new HashMap<>();
            jsonMap.put("username", username);
            jsonMap.put("password", password);

            HttpEntity<Map<String, String>> request = new HttpEntity<>(jsonMap, headers);
            System.out.println("Using " + auth_api_url);
            ResponseEntity<String> response = restTemplate.postForEntity(auth_api_url, request, String.class);

            return response.getStatusCode();
        } catch (HttpClientErrorException e) {
            //e.printStackTrace();
            return e.getStatusCode();
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public HttpStatus authenticateUserAndPost(UUID post_id, String username, String password) {
        Post post = postService.findPost(post_id);
        if (post.getUserRef().equals(username)) {
            return authenticateUser(username, password);
        } else {
            return HttpStatus.FORBIDDEN;
        }
    }

}