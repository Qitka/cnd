package de.thi.inf.sesa.hexa.ports.outgoing;

import org.springframework.http.HttpStatus;

import java.util.UUID;

public interface AuthAdapter {
    HttpStatus authenticateUser(String username, String password);
    HttpStatus authenticateUserAndPost(UUID post_id, String username, String password);
}