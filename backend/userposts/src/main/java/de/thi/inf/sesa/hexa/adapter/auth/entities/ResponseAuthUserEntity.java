package de.thi.inf.sesa.hexa.adapter.auth.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ResponseAuthUserEntity {
    private String username;
    private String password;
}
