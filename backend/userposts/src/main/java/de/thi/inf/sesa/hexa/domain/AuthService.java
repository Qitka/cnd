package de.thi.inf.sesa.hexa.domain;

import org.springframework.http.HttpStatus;

import java.util.UUID;

public interface AuthService {
    HttpStatus authenticate(UUID post_id, String username, String password);
    HttpStatus authenticateUserAndPost(UUID post_id, String username, String password);
}