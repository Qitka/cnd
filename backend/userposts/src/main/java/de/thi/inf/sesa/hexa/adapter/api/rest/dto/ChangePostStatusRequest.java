package de.thi.inf.sesa.hexa.adapter.api.rest.dto;

import de.thi.inf.sesa.hexa.domain.model.AuthUser;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class ChangePostStatusRequest {
    private UUID id;
    private AuthUser authuser;
    private String title;
}