package de.thi.inf.sesa.hexa.adapter.api.rest;

import de.thi.inf.sesa.hexa.adapter.api.rest.dto.CreatePostRequest;
import de.thi.inf.sesa.hexa.adapter.api.rest.dto.EditPostRequest;
import de.thi.inf.sesa.hexa.adapter.api.rest.dto.ListPostsByUserRequest;
import de.thi.inf.sesa.hexa.adapter.api.rest.dto.PostResponse;
import de.thi.inf.sesa.hexa.domain.AuthService;
import de.thi.inf.sesa.hexa.domain.PostService;
import de.thi.inf.sesa.hexa.domain.Status;
import de.thi.inf.sesa.hexa.domain.model.UserReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import de.thi.inf.sesa.hexa.domain.model.Post;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/posts")
public class PostController {
    @Autowired
    private final PostService service;

    @Autowired
    private final AuthService authenticator;

    // Constructor for dependency injection
    @Autowired
    public PostController(PostService service, AuthService authenticator) {
        this.service = service;
        this.authenticator = authenticator;
    }

    @PostMapping
    public ResponseEntity<PostResponse> createPost(@RequestBody CreatePostRequest request) {
        HttpStatus authRes = authenticator.authenticate(null, request.getAuthuser().getUsername(), request.getAuthuser().getPassword());
        if (authRes != HttpStatus.OK) {
            return ResponseEntity.status(authRes).build();
        }
        Post p = service.createPost(request.getTitle(), request.getContent(), request.getStatus(), request.getAuthuser().getUsername());
        return ResponseEntity.ok(new PostResponse(p));
    }

    @PostMapping("/edit")
    public ResponseEntity<PostResponse> editPost(@RequestBody EditPostRequest request) {
        HttpStatus authRes = authenticator.authenticateUserAndPost(request.getId(), request.getAuthuser().getUsername(), request.getAuthuser().getPassword());
        if (authRes != HttpStatus.OK) {
            return ResponseEntity.status(authRes).build();
        }
        Post p = service.editPost(request.getId(), request.getTitle(), request.getContent(), request.getStatus(), request.getAuthuser().getUsername());
        return ResponseEntity.ok(new PostResponse(p));
    }

    @PostMapping("/editstatus")
    public ResponseEntity<PostResponse> editPostStatus(@RequestBody EditPostRequest request) {
        HttpStatus authRes = authenticator.authenticateUserAndPost(request.getId(), request.getAuthuser().getUsername(), request.getAuthuser().getPassword());
        if (authRes != HttpStatus.OK) {
            return ResponseEntity.status(authRes).build();
        }
        Post p = service.changeStatus(request.getId(), request.getStatus(), request.getAuthuser().getUsername());
        return ResponseEntity.ok(new PostResponse(p));
    }

    @GetMapping
    public List<PostResponse> getPosts() {
        List<Post> list = service.getPosts();
        List<PostResponse> posts = new ArrayList<>();
        for(Post p : list) {
            if(p.getStatus().equals(Status.PUBLISHED))
                posts.add(new PostResponse(p));
        }
        return posts;
    }

    @PostMapping("/myposts")
    public ResponseEntity<List<PostResponse>> listPostsByUser(@RequestBody ListPostsByUserRequest request) {
        HttpStatus authRes = authenticator.authenticate(null, request.getAuthuser().getUsername(), request.getAuthuser().getPassword());
        if (authRes != HttpStatus.OK) {
            return ResponseEntity.status(authRes).build();
        }

        String userRef = request.getUserRef();
        List<Post> list = service.getPosts();
        List<PostResponse> posts = new ArrayList<>();

        for (Post p : list) {
            if (Objects.equals(p.getUserRef(), userRef)) {
                if (p.getStatus() == Status.DRAFT || p.getStatus() == Status.PUBLISHED) {
                    posts.add(new PostResponse(p));
                }
            }
        }
        return ResponseEntity.ok(posts);
    }

    @GetMapping("/{id}")
    public PostResponse findPost(@PathVariable String id) {
        Post p = service.findPost(UUID.fromString(id));
        return new PostResponse(p);
    }
}