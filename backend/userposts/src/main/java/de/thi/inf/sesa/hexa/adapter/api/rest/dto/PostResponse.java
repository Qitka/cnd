package de.thi.inf.sesa.hexa.adapter.api.rest.dto;

import de.thi.inf.sesa.hexa.domain.Status;
import de.thi.inf.sesa.hexa.domain.model.Post;
import de.thi.inf.sesa.hexa.domain.model.UserReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostResponse {
    private UUID id;
    private String title;
    private String content;
    private Status status;
    private LocalDateTime date;
    private LocalDateTime validUntil;
    private String userRef;

    public PostResponse(Post p) {
        this.id = p.getId();
        this.title = p.getTitle();
        this.content = p.getContent();
        this.status = p.getStatus();
        this.date = p.getDate();
        this.validUntil = p.getValidUntil();
        this.userRef = p.getUserRef();
    }
}