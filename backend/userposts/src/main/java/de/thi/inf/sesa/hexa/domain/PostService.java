package de.thi.inf.sesa.hexa.domain;

import de.thi.inf.sesa.hexa.domain.model.Post;
import de.thi.inf.sesa.hexa.domain.model.UserReference;

import java.util.List;
import java.util.UUID;

public interface PostService {
    Post createPost(String title, String content, Status status, String userRef);
    Post editPost(UUID id, String title, String content, Status status, String userRef);
    Post changeStatus(UUID id, Status status, String userRef);
    Post findPost(UUID id);
    List<Post> getPosts();
}