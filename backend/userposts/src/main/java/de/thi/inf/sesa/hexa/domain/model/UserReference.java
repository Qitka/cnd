package de.thi.inf.sesa.hexa.domain.model;

import lombok.*;

import javax.persistence.Column;
import java.util.UUID;

@Data
@AllArgsConstructor
public class UserReference {
    @Setter(AccessLevel.NONE)
    private final String username;
}