package de.thi.inf.sesa.hexa.application;

import de.thi.inf.sesa.hexa.adapter.auth.AuthController;
import de.thi.inf.sesa.hexa.domain.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private AuthController auth;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public HttpStatus authenticate(UUID post_id, String username, String password) {
        return auth.authenticateUser(username, password);
    }

    @Override
    public HttpStatus authenticateUserAndPost(UUID post_id, String username, String password) {
        return auth.authenticateUserAndPost(post_id, username, password);
    }
}