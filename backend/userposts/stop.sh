#!/bin/bash

# Define an array of container names
containers=("db" "app" "proxy" "broker")

# Loop through each container
for container in "${containers[@]}"; do
    echo "Stopping container: $container"
    docker stop "$container" || echo "Failed to stop container: $container"

    echo "Removing container: $container"
    docker rm "$container" || echo "Failed to remove container: $container"
done

echo "All specified containers have been stopped and removed."