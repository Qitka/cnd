package info.bondar.chat;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.runner.RunWith;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@RunWith(ArchUnitRunner.class) // Remove this line for JUnit 5!!
@AnalyzeClasses(packages = "info.bondar.chat")
public class DependencyTest {
    @ArchTest
    static final ArchRule REST_SHOULD_NOT_ACCESS_JPA = noClasses().that().resideInAPackage("..adapter.in.api.rest..")
            .should().accessClassesThat().resideInAPackage("..adapter.out.jpa..");

    @ArchTest
    static final ArchRule JPA_SHOULD_NOT_ACCESS_REST = noClasses().that().resideInAPackage("..adapter.out.jpa..")
            .should().accessClassesThat().resideInAPackage("..adapter.in.api.rest..");

    @ArchTest
    static final ArchRule DOMAIN_SHOULD_NOT_DEPEND_ON_ADAPTERS = noClasses().that().resideInAPackage("..domain..")
            .should().dependOnClassesThat().resideInAnyPackage("..adapter..");

    @ArchTest
    static final ArchRule JPA_SHOULD_NOT_ACCESS_WEBSOCKET = noClasses().that().resideInAPackage("..adapter.out.jpa..")
            .should().accessClassesThat().resideInAPackage("..adapter.in.ws.config..");

    @ArchTest
    static final ArchRule WEBSOCKET_SHOULD_NOT_ACCESS_JPA = noClasses().that().resideInAPackage("..adapter.in.ws.config..")
            .should().accessClassesThat().resideInAPackage("..adapter.out.jpa..");
}
