package info.bondar.chat;

import info.bondar.chat.adapter.out.entity.MessageEntity;
import info.bondar.chat.adapter.out.entity.UserEntity;
import info.bondar.chat.adapter.out.jpa.JpaUserCrudRepository;
import info.bondar.chat.application.MessageServiceImpl;
import info.bondar.chat.application.out.UserRepositoryInterface;
import info.bondar.chat.domain.model.Chat;
import info.bondar.chat.domain.model.History;
import info.bondar.chat.application.out.MessageRepositoryInterface;
import info.bondar.chat.domain.model.Message;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

public class MessageServiceImplTest {
    MessageRepositoryInterface messageRepository = mock();

    UserRepositoryInterface userRepository = mock();
    MessageServiceImpl messageService = new MessageServiceImpl(messageRepository, userRepository);
    Message message = mock(Message.class);
    History history = mock(History.class);
    Chat chat = mock(Chat.class);

    @Test
    public void testSaveMessage() {
        // Given
        Message message = mock(Message.class);
        when(message.getSender()).thenReturn("sender");
        when(message.getReceiver()).thenReturn("receiver");

        UserEntity senderEntity = new UserEntity();
        senderEntity.setUsername(message.getSender());

        UserEntity receiverEntity = new UserEntity();
        receiverEntity.setUsername(message.getReceiver());

        when(userRepository.findByUsername(message.getSender())).thenReturn(senderEntity);
        when(userRepository.findByUsername(message.getReceiver())).thenReturn(receiverEntity);

        MessageEntity messageEntity = new MessageEntity();
        when(messageRepository.save(any(MessageEntity.class))).thenReturn(messageEntity);

        // When
        Message result = messageService.saveMessage(message);

        // Then
        assertEquals(message, result);
        verify(userRepository, times(2)).findByUsername(anyString());
        verify(messageRepository, times(2)).save(any(MessageEntity.class));
    }


    @Test
    public void testGetChatHistory() {
        // Given
        String username = "username";
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setUsername(username);

        List<MessageEntity> messageEntities = new ArrayList<>();
        MessageEntity messageEntity = new MessageEntity();
        messageEntity.setSender(userEntity);
        messageEntity.setReceiver(userEntity);
        messageEntity.setMessage("message");
        messageEntities.add(messageEntity);

        when(userRepository.findByUsername(username)).thenReturn(userEntity);
        when(messageRepository.findByUserId(userEntity.getId())).thenReturn(messageEntities);

        // When
        History result = messageService.getChatHistory(username);

        // Then
        assertNotNull(result);
        assertEquals(username, result.getUsername());
        assertEquals(1, result.getChats().size());

        Chat chat = result.getChats().get(0);
        assertEquals(username, chat.getUsername());
        assertEquals(1, chat.getMessage().size());

        Message resultMessage = chat.getMessage().get(0);
        assertEquals(messageEntity.getMessage(), resultMessage.getMessage());
    }


}
