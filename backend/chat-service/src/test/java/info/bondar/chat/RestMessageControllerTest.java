package info.bondar.chat;

import info.bondar.chat.adapter.in.api.rest.RestMessageController;
import info.bondar.chat.application.in.MessageServiceInterface;
import info.bondar.chat.application.in.UserServiceInterface;
import info.bondar.chat.domain.model.History;
import info.bondar.chat.domain.model.Message;
import info.bondar.chat.domain.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.Assert.assertEquals;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

class RestMessageControllerTest {


	MessageServiceInterface messageService = mock();

	UserServiceInterface userService = mock();
	SimpMessagingTemplate simpMessagingTemplate = mock();

	Message message = mock(Message.class);

	History history = mock(History.class);

	User user = mock(User.class);


	RestMessageController restMessageController = new RestMessageController(simpMessagingTemplate, messageService, userService);


	@Test
	public void testReceiveMessage() {
		when(message.getReceiver()).thenReturn("receiver");

		// When
		Message result = restMessageController.receiveMessage(message);

		// Then
		Assertions.assertEquals(message, result);
		verify(simpMessagingTemplate, times(1)).convertAndSendToUser(anyString(), anyString(), any());
	}

	@Test
	public void testSaveMessage() {
		String username = "username";
		when(messageService.saveMessage( message)).thenReturn(message);

		ResponseEntity<Message> result = restMessageController.saveMessage(message);

		verify(messageService, times(1)).saveMessage(message);
		Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assertions.assertEquals(message, result.getBody());
	}

	@Test
	public void testGetChatHistory() {

		String username = "username";
		when(messageService.getChatHistory(username)).thenReturn(history);
		when(history.getChats()).thenReturn(Collections.emptyList());

		ResponseEntity<History> result = restMessageController.getChatHistory(username);

		verify(messageService, times(1)).getChatHistory(username);
		Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assertions.assertNotEquals(history, result.getBody());
		Assertions.assertEquals(username, Objects.requireNonNull(result.getBody()).getUsername());
		Assertions.assertTrue(result.getBody().getChats().isEmpty());
	}

	@Test
	public void testUpdateUserTable() {
		doNothing().when(userService).updateUserTable(user);

		// When
		ResponseEntity<String> result = restMessageController.updateUserTable(user);

		// Then
		verify(userService, times(1)).updateUserTable(user);
		Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assertions.assertEquals("User updated", result.getBody());
	}


	@Test
	public void testGetUsers() {
		// Scenario 1: Users exist
		String user1 = "user1";
		String user2 = "user2";
		List<String> users = Arrays.asList(user1, user2);
		when(userService.getUsers()).thenReturn(users);

		ResponseEntity<List<String>> result = restMessageController.getUsers();
		verify(userService, times(1)).getUsers();
		Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assertions.assertEquals(users, result.getBody());

		// Scenario 2: No users exist
		when(userService.getUsers()).thenReturn(Collections.emptyList());

		result = restMessageController.getUsers();
		verify(userService, times(2)).getUsers();
		Assertions.assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
	}


}
