package info.bondar.chat;
import info.bondar.chat.application.UserServiceImpl;
import info.bondar.chat.application.out.UserRepositoryInterface;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;

import info.bondar.chat.application.in.UserServiceInterface;
import info.bondar.chat.domain.model.User;
import info.bondar.chat.adapter.out.jpa.JpaUserCrudRepository;
import info.bondar.chat.adapter.out.entity.UserEntity;
import info.bondar.chat.application.out.UserAdapterInterface;

class UserServiceImplTest {


    UserRepositoryInterface userRepository = mock();

    UserAdapterInterface userAdapterInterface = mock();

    UserServiceInterface userService = new UserServiceImpl(userRepository, userAdapterInterface);

    User user = mock(User.class);

    UserEntity existingUser = mock(UserEntity.class);

    @Test
    public void testUpdateUserTable() {

        existingUser.setUsername(user.getUsername());

        when(userRepository.findByUsername(user.getUsername())).thenReturn(existingUser);

        userService.updateUserTable(user);

        verify(userRepository, times(1)).save(any(UserEntity.class));
    }

    @Test
    public void testGetUsers() {
        UserEntity userEntity1 = mock(UserEntity.class);

        UserEntity userEntity2 = mock(UserEntity.class);

        List<UserEntity> userEntities = Arrays.asList(userEntity1, userEntity2);

        when(userRepository.findAll()).thenReturn(userEntities);

        List<String> result = userService.getUsers();

        assertEquals(userEntities.size(), result.size());
        assertTrue(result.contains(userEntity1.getUsername()));
        assertTrue(result.contains(userEntity2.getUsername()));
    }
}
