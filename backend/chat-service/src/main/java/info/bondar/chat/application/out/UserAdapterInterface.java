package info.bondar.chat.application.out;

import java.util.List;

import info.bondar.chat.domain.model.User;

public interface UserAdapterInterface {
    List<User> getAllUsers();
}
