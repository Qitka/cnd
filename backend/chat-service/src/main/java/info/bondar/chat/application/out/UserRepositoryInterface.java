package info.bondar.chat.application.out;

import info.bondar.chat.adapter.out.entity.UserEntity;

import java.util.List;

public interface UserRepositoryInterface {
    UserEntity findByUsername(String username);

    void save(UserEntity entity);

    List<UserEntity> findAll();
}
