package info.bondar.chat.adapter.out.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import info.bondar.chat.domain.model.User;
import info.bondar.chat.application.out.UserAdapterInterface;

@Service
public class UserAdapter implements UserAdapterInterface {

    private final RestTemplate restTemplate;

    private final String userServiceUrl;

    public UserAdapter(RestTemplate restTemplate, @Value("${services.user-service}") String userServiceUrl) {
        this.restTemplate = restTemplate;
        this.userServiceUrl = userServiceUrl;
    }

    public List<User> getAllUsers() {
        try {
            // Make a GET request to the user service to fetch all users
            ResponseEntity<List<User>> response = restTemplate.exchange(
                userServiceUrl + "/api/v1/user/users",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {});

            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            } else {
                System.out.println("Failed to fetch users from user service");
            }
        } catch (RestClientException e) {
            System.out.println("User service is currently offline, continuing without updating user table");
        }

        return new ArrayList<>();
    }

}
