package info.bondar.chat.domain.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
public class User {
    private Long id;
    private String username;
    private String email;
}
