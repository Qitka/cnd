package info.bondar.chat.application.in;

import info.bondar.chat.domain.model.History;
import info.bondar.chat.domain.model.Message;

public interface MessageServiceInterface {


    Message saveMessage(Message message);

    History getChatHistory(String username);

}
