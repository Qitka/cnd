package info.bondar.chat.application.out;

import info.bondar.chat.adapter.out.entity.MessageEntity;
import info.bondar.chat.adapter.out.entity.UserEntity;

import java.util.List;

public interface MessageRepositoryInterface {
    MessageEntity save(MessageEntity messageEntity);

    List<MessageEntity> findByUser(UserEntity user);

    List<MessageEntity> findByUserId(Long userId);
}
