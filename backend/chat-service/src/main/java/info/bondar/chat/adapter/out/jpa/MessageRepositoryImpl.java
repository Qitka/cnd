package info.bondar.chat.adapter.out.jpa;

import info.bondar.chat.adapter.out.entity.MessageEntity;
import info.bondar.chat.adapter.out.entity.UserEntity;
import info.bondar.chat.application.out.MessageRepositoryInterface;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageRepositoryImpl implements MessageRepositoryInterface {

    private final JpaMessageCrudRepository jpaMessageCrudRepository;

    public MessageRepositoryImpl(JpaMessageCrudRepository jpaMessageCrudRepository) {
        this.jpaMessageCrudRepository = jpaMessageCrudRepository;
    }

    @Override
    public MessageEntity save(MessageEntity messageEntity) {
        return jpaMessageCrudRepository.save(messageEntity);
    }

    @Override
    public List<MessageEntity> findByUser(UserEntity user) {
        return jpaMessageCrudRepository.findByUser(user);
    }

    @Override
    public List<MessageEntity> findByUserId(Long userId) {
        return jpaMessageCrudRepository.findByUserId(userId);
    }


}
