package info.bondar.chat.application.in;

import info.bondar.chat.domain.model.User;

import java.util.List;

public interface UserServiceInterface {

    void updateUserTable(User user);

    void updateAllUsers();

    List<String> getUsers();
}
