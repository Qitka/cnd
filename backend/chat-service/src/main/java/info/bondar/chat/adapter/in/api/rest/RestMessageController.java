package info.bondar.chat.adapter.in.api.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import info.bondar.chat.application.in.MessageServiceInterface;
import info.bondar.chat.application.in.UserServiceInterface;
import info.bondar.chat.domain.model.History;
import info.bondar.chat.domain.model.Message;
import info.bondar.chat.domain.model.User;

@RestController
@RequestMapping("/api/v1/chat")
public class RestMessageController {

    private final SimpMessagingTemplate simpMessagingTemplate;

    private final MessageServiceInterface messageService;

    private final UserServiceInterface userService;

    public RestMessageController(SimpMessagingTemplate simpMessagingTemplate, MessageServiceInterface messageService,
        UserServiceInterface userService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.messageService = messageService;
        this.userService = userService;
    }

    @MessageMapping("/private-message")
    public Message receiveMessage(@Payload Message message){
        try {
            simpMessagingTemplate.convertAndSendToUser(message.getReceiver(),"/private", message); // /user/{username}/private
        } catch (Exception e) {
            System.out.println("Error sending message: " + e.getMessage());
        }
        return message;
    }

    @PostMapping("/save-message")
    public ResponseEntity<Message> saveMessage(@RequestBody Message message) {
        Message savedMessage = messageService.saveMessage(message);
        if (savedMessage != null) {
            return new ResponseEntity<>(savedMessage, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{username}")
    public ResponseEntity<History> getChatHistory(@PathVariable String username) {
        History history = messageService.getChatHistory(username);
        if (history != null && !history.getChats().isEmpty()) {
            return new ResponseEntity<>(history, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new History(username, List.of()), HttpStatus.OK);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<String> updateUserTable(@RequestBody User user) {
        try {
            userService.updateUserTable(user);
            return ResponseEntity.ok("User updated");
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-users")
    public ResponseEntity<List<String>> getUsers() {
        List<String> userList = userService.getUsers();
        if (userList != null && !userList.isEmpty()) {
            return ResponseEntity.ok(userList);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
