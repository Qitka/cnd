package info.bondar.chat.application;

import info.bondar.chat.adapter.out.entity.MessageEntity;
import info.bondar.chat.adapter.out.entity.UserEntity;
import info.bondar.chat.adapter.out.jpa.JpaUserCrudRepository;
import info.bondar.chat.application.out.UserRepositoryInterface;
import info.bondar.chat.domain.model.Chat;
import info.bondar.chat.domain.model.History;
import info.bondar.chat.application.out.MessageRepositoryInterface;
import info.bondar.chat.application.in.MessageServiceInterface;
import info.bondar.chat.domain.model.Message;

import org.springframework.stereotype.Service;


import java.util.*;

@Service
public class MessageServiceImpl implements MessageServiceInterface {

    private final MessageRepositoryInterface messageRepository;

    private final UserRepositoryInterface userRepository;

    public MessageServiceImpl(MessageRepositoryInterface messageRepository, UserRepositoryInterface userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Message saveMessage(Message message) {
        String messageId = UUID.randomUUID().toString();

        UserEntity sender = userRepository.findByUsername(message.getSender());
        if (sender == null) {
            return null;
        }

        UserEntity receiver = userRepository.findByUsername(message.getReceiver());
        if (receiver == null) {
            return null;
        }

        // Create and save a MessageEntity for the sender
        MessageEntity senderMessage = new MessageEntity();
        senderMessage.setUser(sender);
        senderMessage.setSender(sender);
        senderMessage.setReceiver(receiver);
        senderMessage.setMessage(message.getMessage());
        senderMessage.setMessageId(messageId); // Set the chatId
        messageRepository.save(senderMessage);

        // Create and save a MessageEntity for the receiver
        MessageEntity receiverMessage = new MessageEntity();
        receiverMessage.setUser(receiver);
        receiverMessage.setSender(sender);
        receiverMessage.setReceiver(receiver);
        receiverMessage.setMessage(message.getMessage());
        receiverMessage.setMessageId(messageId); // Set the chatId
        messageRepository.save(receiverMessage);

        return message;
    }

    public History getChatHistory(String username) {
        UserEntity userEntity = userRepository.findByUsername(username);
        Long userId = null;
        if (userEntity != null) {
            userId = userEntity.getId();
        }

        List<MessageEntity> messageEntities = new ArrayList<>();
        if (userId != null) {
            messageEntities = messageRepository.findByUserId(userId);
        }

        Map<String, List<Message>> messages = new HashMap<>();

        for (MessageEntity messageEntity : messageEntities) {
            boolean isReceiver = messageEntity.getReceiver().getId() == userEntity.getId();
            boolean isSender = messageEntity.getSender().getId() == userEntity.getId();
            if (isReceiver || isSender) {
                String chatWith;
                if (isSender) {
                    chatWith = messageEntity.getReceiver().getUsername();
                } else {
                    chatWith = messageEntity.getSender().getUsername();
                }

                Message message = new Message(
                    messageEntity.getSender().getUsername(),
                    messageEntity.getReceiver().getUsername(),
                    messageEntity.getMessage()
                );

                if (messages.get(chatWith) == null) {
                    messages.put(chatWith, new ArrayList<>(Arrays.asList(message)));
                } else {
                    messages.get(chatWith).add(message);
                }
            }
        }

        return new History(username, messages.entrySet().stream().map((entry) -> new Chat(entry.getKey(), entry.getValue())).toList());
    }

}



