package info.bondar.chat.application;

import info.bondar.chat.adapter.out.entity.UserEntity;
import info.bondar.chat.adapter.out.jpa.JpaUserCrudRepository;
import info.bondar.chat.application.in.UserServiceInterface;
import info.bondar.chat.application.out.UserRepositoryInterface;
import info.bondar.chat.domain.model.User;
import info.bondar.chat.application.out.UserAdapterInterface;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserServiceInterface {

    private final UserRepositoryInterface userRepository;

    private final UserAdapterInterface userAdapter;

    public UserServiceImpl(UserRepositoryInterface userRepository, UserAdapterInterface userAdapter) {
        this.userRepository = userRepository;
        this.userAdapter = userAdapter;
    }

    @Override
    public void updateUserTable(User user) {
        UserEntity existingUser = userRepository.findByUsername(user.getUsername());

        if (existingUser != null) {
            existingUser.setEmail(user.getEmail());
            userRepository.save(existingUser);
        } else {
            UserEntity newUserEntity = new UserEntity();
            newUserEntity.setUsername(user.getUsername());
            newUserEntity.setEmail(user.getEmail());
            userRepository.save(newUserEntity);
        }
    }

    @Override
    public void updateAllUsers() {
        List<User> users = userAdapter.getAllUsers();
        for(User user : users) {
            this.updateUserTable(user);
        }
    }

    @Override
    public List<String> getUsers() {
        List<UserEntity> userEntities = StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .toList();

        List<String> usernames = userEntities.stream()
                .map(UserEntity::getUsername)
                .collect(Collectors.toList());

        return usernames;
    }
}
