package info.bondar.chat.adapter.out.jpa;

import info.bondar.chat.adapter.out.entity.UserEntity;
import info.bondar.chat.application.out.UserRepositoryInterface;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class UserRepositoryImpl implements UserRepositoryInterface {

    private final JpaUserCrudRepository jpaUserCrudRepository;

    public UserRepositoryImpl(JpaUserCrudRepository jpaUserCrudRepository) {
        this.jpaUserCrudRepository = jpaUserCrudRepository;
    }


    public UserEntity findByUsername(String username){
        return jpaUserCrudRepository.findByUsername(username);

    }

    @Override
    public void save(UserEntity entity) {
        jpaUserCrudRepository.save(entity);
    }

    @Override
    public List<UserEntity> findAll() {
        List<UserEntity> users = new ArrayList<>();
        Iterable<UserEntity> itr = jpaUserCrudRepository.findAll();
        itr.forEach(item -> users.add(item));
        return users;
    }
}
