package info.bondar.chat.adapter.out.jpa;

import info.bondar.chat.adapter.out.entity.MessageEntity;
import info.bondar.chat.adapter.out.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JpaUserCrudRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByUsername(String username);

}
