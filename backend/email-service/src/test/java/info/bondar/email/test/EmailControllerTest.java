package info.bondar.email.test;

import info.bondar.email.test.controller.EmailController;
import info.bondar.email.test.model.EmailModel;
import info.bondar.email.test.service.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


class EmailControllerTest {

    EmailService emailService = mock();

    EmailController emailController = new EmailController(emailService);

    @Test
    void sendEmail() {
        String email = "test@example.com";
        EmailModel emailModel = new EmailModel();

        when(emailService.sendEmail(emailModel)).thenReturn("Email sent to " + email);

        ResponseEntity<String> result = emailController.sendEmail(emailModel);

        verify(emailService, times(1)).sendEmail(emailModel);
        assertEquals(ResponseEntity.ok("Email sent to " + email), result);
    }
}
