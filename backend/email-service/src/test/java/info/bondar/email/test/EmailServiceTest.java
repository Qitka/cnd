package info.bondar.email.test;

import info.bondar.email.test.model.EmailModel;
import info.bondar.email.test.service.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import static org.mockito.Mockito.doNothing;

class EmailServiceTest {

    String sender = "sender";

    JavaMailSender mailSender = mock();

    EmailService emailService = new EmailService(mailSender, sender);


    @Test
    void sendEmail() {
        String email = "test@example.com";
        String username = "test";
        EmailModel emailModel = new EmailModel();
        doNothing().when(mailSender).send(any(SimpleMailMessage.class));

        emailService.sendEmail(emailModel);

        verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
    }
}
