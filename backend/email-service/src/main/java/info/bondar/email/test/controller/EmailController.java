package info.bondar.email.test.controller;

import info.bondar.email.test.model.EmailModel;
import info.bondar.email.test.service.EmailService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/email")
public class EmailController {

    private final EmailService emailService;

    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/send")
    public ResponseEntity<String> sendEmail(@RequestBody EmailModel emailModel){
        String response = emailService.sendEmail(emailModel);
        return ResponseEntity.ok(response);
    }
}
