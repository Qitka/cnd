package info.bondar.email.test.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
public class EmailModel {
    private String email;
    private String subject;
    private String message;
}
