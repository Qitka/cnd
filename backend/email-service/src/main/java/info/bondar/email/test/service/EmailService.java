package info.bondar.email.test.service;

import info.bondar.email.test.model.EmailModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    private final JavaMailSender mailSender;

    private final String sender;

    public EmailService(JavaMailSender mailSender, @Value("${spring.mail.username}") String sender) {
        this.mailSender = mailSender;
        this.sender = sender;
    }

    public String sendEmail(EmailModel emailModel){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        System.out.println("Sending email: " + emailModel);

        simpleMailMessage.setFrom(sender);
        simpleMailMessage.setTo(emailModel.getEmail());
        simpleMailMessage.setSubject(emailModel.getSubject());
        simpleMailMessage.setText(emailModel.getMessage());

        mailSender.send(simpleMailMessage);

        return emailModel.getMessage();
    }
}
