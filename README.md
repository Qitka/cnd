# Cloud-native Development (CND) Wintersemester 2023/24

## Requirement 1 (German):
Das Anwendungsbeispiel muss je Team-Mitglied mindestens aus zwei (Backend-) Diensten bestehen und Daten erfassen (direkt im Dateisystem oder über ein Datenbankmanagementsystem), ein Dienst sollte Anfragen über HTTP entgegen nehmen.

Fulfillment:
* user-service + MySQL Database (by Larysa)
* email-service (by Larysa)
* chat-service + mySQL Database (by Larysa)
* userposts + mySQL Database (by Fabian)
* rating-service - SQLite (by Fabian)

## Requirement 2 (German):
Realisieren Sie je Team-Mitglied mindestens einen Dienst unter Verwendung eines modernen Architekturansatzes (Hexagonal, Onion, Clean Architecture, ...).

Fulfillment:
* chat-service - hexagonal Architecture. For the Architecture Diagramm refer to `./documentation/chat-service.jpeg` (by Larysa)
* userposts - hexagonal Architecture. For the Architecture Diagramm refer to `./documentation/userposts.jpeg` (by Fabian)

## Requirement 3 (German):
Ergänzen Sie einen kleines Frontend, welches das realisierte Backend-Funktionalität nutzt. Eine Webseite mit einer minimalen GUI zur Nutzung der HTTP-basierten Schnittstelle Ihres Backends ohne Anspruch an Layout ist ausreichend.

Fulfillment:
Frontend is accessible at:
* local execution: `http://localhost:3000`
* docker containers: `http://localhost:8080`
* docker compose: `http://localhost:8080`
* kubernetes: `http://localhost:80`

## Requirement 4 (German)
Ergänzen Sie für die Dienste einfache Unit-Tests, die den jeweiligen Dienst überprüfen, eine vollständige Test-Abdeckung ist nicht notwendig.

Fulfillment:
Unit tests are to find at:
* user-service: `cnd\backend\user-service\src\test\java\info\bondar\user`
* email-service: `cnd\backend\email-service\src\test\java\info\bondar\email`
* chat-service: `cnd\backend\chat-service\src\test\java\info\bondar\chat`
* userposts: `cnd\backend\userposts\src\test`
* rating-service: `cnd\backend\rating\test.py`


## Requirement 5 (German):
Dokumentieren Sie kurz, was nötig ist, um die Dienste in einer VM zu installieren und zu starten.

Fulfillment:

### Run Code Locally
Tested on:
- Ubuntu 22.04.3 LTS (Jammy Jellyfish) ([AMD 64 bit, netinst](https://releases.ubuntu.com/jammy/)) inside VirtualBox (6.1.48) VM
- OpenJDK JDK 21.0.1 [General-Availability Release](https://www.oracle.com/java/technologies/downloads/) (Linux/x64 Debian package)
  - [Installation guide (unofficial)](https://ubuntuhandbook.org/index.php/2022/03/install-jdk-18-ubuntu/)
- MySQL [Installation guide](https://ubuntu.com/server/docs/databases-mysql)

  - Install MySQL:
    ```
      sudo apt install mysql-server
      sudo systemctl status mysql.service
    ```

    - If status is not "active", run
   
      `sudo systemctl start mysql.service`

  - Set the root username and password
    ```
  	  sudo mysql
      > ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '1245+3265';
      > exit
    ```

  - To access the database, use this command:

    `sudo mysql -u root -p`

    (Optional) Install MySQL Workbench:

    `sudo snap install mysql-workbench-community`

Execution:
- Run mySQL databse on localhost:3306 with user `root` and password `1245+3265`
- Run user-service
  - `cd backend/user-service`
  - `chmod +x ./mvnw`
  - `./mvnw install`
  - `java -jar target/user-0.0.1-SNAPSHOT.jar`
- Run email-service
  - `cd backend/email-service`
  - `chmod +x ./mvnw`
  - `./mvnw install`
  - `java -jar target/email-0.0.1-SNAPSHOT.jar`
- Run chat-service
  - `cd backend/chat-service`
  - `chmod +x ./mvnw`
  - `./mvnw install`
  - `java -jar target/chat-0.0.1-SNAPSHOT.jar`
- Run userposts
  - `cd backend/userposts`
  - `chmod +x ./mvnw`
  - `./mvnw install`
  - `java -jar target/userposts-0.0.1-SNAPSHOT.jar`
- Run rating
  - `cd backend/rating`
  - `apt install python3-pip`
  - `pip install -r requirements.txt`
  - `python3 app.py`
- Run frontend
  - `cd frontend`
  - `sudo apt install npm`
  - `npm install`
  - `npm run start`

## Requirement 6 (German):
Erstellen Sie Dockerfiles, welches je einen Dienst zur Ausführung bringt und Dokumentieren Sie, wie diese Dienste installiert und gestartet werden. Achten Sie auf Nutzdaten und eine geeignete Strategie, damit beim neuen Erzeugen des Containers auf bestehenden Daten gearbeitet werden kann.

Fulfillment:
The Dockerfiles are to find at:
- user-service: `cnd\backend\user-service\Dockerfile`
- email-service: `cnd\backend\email-service\Dockerfile`
- chat-service: `cnd\backend\chat-service\Dockerfile`
- userposts: `cnd\backend\userposts\Dockerfile`
- rating: `cnd\backend\rating\Dockerfile`


### Run Code in Docker

- Install docker [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- Create network with

`sudo docker network create cnd-network`

- Run mysql

`sudo docker run --name mysql -e MYSQL_ROOT_PASSWORD="1245+3265" -p 3306:3306 --network cnd-network -v /$(pwd)/mount/mysql:/var/lib/mysql -d mysql:8.2.0`

- Run user-service

`sudo docker run -p 9191:9191 --env-file ./config/docker.env --name user-service --network cnd-network qitka/user-service:latest`

- Run email-service

`sudo docker run -p 9192:9192 --env-file ./config/docker.env --name email-service --network cnd-network qitka/email-service:latest`

- Run chat-service

`sudo docker run -p 9193:9193 --env-file ./config/docker.env --name chat-service --network cnd-network qitka/chat-service:latest`

- Run userposts

`sudo docker run -p 8080:9194 --env-file ./config/docker.env --name userposts --network cnd-network fas9014/post-service:latest`

- Run rating

`docker run -p 5000:5000 --env-file ./config/docker.env --name rating -v ${PWD}/mount/sqlite:/app/storage --network cnd-network fas9014/rating-service:latest`

- Run frontend

`sudo docker run -p 8080:8080 --env-file ./config/docker.env --name frontend --network cnd-network qitka/frontend:latest`

- Frontend should be accessible at `http://localhost:8080`

## Requirement 7 (German):
Erstellen Sie ein Docker-Compose-File, welche alle Dienste konfiguriert und Dokumentieren Sie, wie diese Datei genutzt wird, ergänzen Sie einen Load-Balancer (z.B. nginx) um Anfragen zu verteilen. Achten Sie auf die Konfiguration Ihrer Volumes.

Fulfillment:
The docker compose file is located at /cnd/

### Run code Using docker-compose

- Install Docker [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- From /cnd/ run

Depending on your installation, run one of the following:

`docker compose --env-file config/docker-compose.env up`

`docker-compose --env-file config/docker-compose.env up`

- Frontend should be accessible at `http://localhost:8080`


## Requirement 8 (German):
Erstellen Sie Kubernetes-Manifeste, welche Ihre Dienste konfiguriert. Verwenden Sie Ingress-Ressourcen, Konfigurationen und Volumes, eine einfache Lösung für Volumes ist ausreichend. Dokumentieren Sie die Verwendung. Kubernetes Persistence Volumes können für die Abgabe weggelassen werden, gehen Sie von einem "storageClassName: standard" für Ihren Claim aus.

Fulfillment:
Kubernetes manifests are located at `/cnd/kubernetes`

### Run code in Kubernetes
Tested on:
- Kubernetes in Docker Desktop on Windows

Preparation:
- Install kubectl: [Kubernetes Documentation](https://kubernetes.io/docs/tasks/tools/)
- Install ingress controller in kubernetes cluster

  - Example: [Ingress-Nginx Installation](https://kubernetes.github.io/ingress-nginx/deploy/)

  - Command tested:

  `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.9.5/deploy/static/provider/cloud/deploy.yaml`

- In mysql-pv.yaml change

  `path: /run/desktop/mnt/host/c/users/larys/Studium/CND/cnd/mount/mysql`

  to

  `/run/desktop/mnt/host/<full path to the mysql volume on your machine>`
- In sqlite-pv.yaml change

  `path: /run/desktop/mnt/host/e/storage/rating`

to

`/run/desktop/mnt/host/<full path to the sqlite volume on your machine>`


Execution:
- To apply all the configurations in the manifest files, in CMD or PowerShell, run

  `kubectl apply -f ./kubernetes/.`

This might take some time (multiple minutes).

Once all services are ready, access the frontend on `http://localhost:80/`
^

Sidenote:
For some magical reason, only in k8s and only during the first registration after starting the app, you will get a 500 error immediately followed by a 200. Each next registration works as expected. 
Hopefully, I will manage to find what causes it.

## Requirement 9 (German):
Automatisieren Sie den Bauprozess in einer wählbaren Umgebung, verwenden Sie hierfür z.B. das GitLab und verfügbare GitLab-CI.

Fulfillment:
The pipeline is located at `/cnd/.gitlab-ci.yml`

## Requirement 10 (German):
Skizzieren Sie die realisierte finale Service-Architektur und die innere Architektur eines Dienstes (realisiert mit einem modernen Architekturansatz) - die Skizzen sind insbesondere für die Abschlussbesprechung relevant.

Fulfillment:
* The Architecture diagram is available at: `/cnd/diagrams/architecture.jpeg`
* The chat-service diagram (by Larysa) is available at: `/cnd/diagrams/chat-service.jpeg`
* The userposts diagram (by Fabian) is available at: `/cnd/diagrams/userposts.jpeg`
